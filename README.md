#Aikyatan

This project has the implementation of the paper 

[Aikyatan : Mapping Distal Regulatory Elements using Convolutional Learning on GPU]
In this paper, we have develpped a suite of machine learning (ML) models, including SVM models, random forest variants, and deep learning architectures such as the vanilla deep neural network (DNN) and the convolutional variant (Convnet or CNN) to annotate distal epigenomic regulatory sites, such as genomic enhancers and insulators. It is challenging to annotate these sites because of the diverse combinatorial epigenomic signatures associated with these distal sites. Plus, it is important to annotate them because they house the highest densities of disease SNPs (single nucloetide polymorphisms). Our best-in-class CNN-based model (Aikyatan-CNN) is not only expressive enough to capture the subtle signatures of these sites but also scales with the exponentially growing epigenomic data volumes. Aikyatan-CNN achieves 40% higher validation rate versus the ANN-based CSIANN and the same accuracy as the random forest-based RFECS.


## Environment

We implemented Aikyatan using [Keras](https://keras.io/) and [scikit-learn](http://scikit-learn.org/stable/).

Now Keras allows [Tensorflow](https://www.tensorflow.org/), [Theano](http://deeplearning.net/software/theano/), and [CNTK](https://www.microsoft.com/en-us/cognitive-toolkit/) as backend support. 

User can install any of these three from the corresponding website in order to enable the software support.


## Dataset

The raw dataset were downloaded from [NCBI Database](https://www.ncbi.nlm.nih.gov/) and [ENCODE](https://genome.ucsc.edu/encode/). 

For the detailed procedure of preprocessing dataset, please refer to our paper's Method Section.

## Running the Script

We have implemented Linear SVM, RBF Kernel SVM, RP-DNN, and RP-CNN.

To run Linear SVM:

```
python SVM_Linear.py TrainX_File TrainY_File TestX_File TestY_File Predtion_Output_Path C_Param
```

To run Kernel SVM:

```
python SVM_Kernel.py TrainX_File TrainY_File TestX_File TestY_File Predtion_Output_Path C_Param Gama_Param
```


To run RP-DNN:

```
python RPDNN.py TrainX_File TrainY_File TestX_File TestY_File Predtion_Output_Path
```

To run RP-CNN:

```
python RPCNN.py TrainX_File TrainY_File TestX_File TestY_File Predtion_Output_Path
```


## License

This project is licensed under the BSD License - see the [LICENSE](LICENSE) file for details
