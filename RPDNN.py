'''
RPDNN.py

Installation Requirement:

    Keras, Theano or Tensorflow 

Requires files:

    train_features.x
    train_labels.y
    test_features.x
    test_labels.y

Author:

    Chih-Hao Fang (fang150@purdue.edu)
'''

from __future__ import print_function
import random
import numpy as np
import evaluate as ev
import utils as stats
import time as tm
import sys
import math
import  keras



from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.embeddings import Embedding
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.layers.advanced_activations import PReLU, ELU,LeakyReLU
from keras.regularizers import l2, activity_l2
from keras.optimizers import SGD, Adam, RMSprop, Adagrad, Adadelta
from theano import config
from StringIO import StringIO
from itertools import islice
from keras.callbacks import ModelCheckpoint

### PARAMS C ###
CELL = 'H1'
N_MODS = 24
N_WINS = 20

F_TRAINX =sys.argv[1]
F_TRAINY =sys.argv[2]

F_LARGE_TESTX =sys.argv[3]
F_LARGE_TESTY =sys.argv[4]

RAW_F = sys.argv[5] 


### DNN MODEL DEFINITION ###
def getDNNModel(input_dim):
    model = Sequential()
    model.add(Dense(600, input_dim=input_dim,
                    init='glorot_normal'  ) )
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(Dense(500,
                    init='glorot_normal' ) )
    model.add(PReLU())
    model.add(Dropout(0.3 ))
    model.add(Dense(400,
                    init='glorot_normal'  ) )
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(Dense(1,
                    init='glorot_normal' ) )
    model.compile(loss='mse', optimizer='RMSprop')
    return model

def removeNaN(X,Y):
    idx = np.where(np.all(np.isnan(X), axis=1))
    X = np.delete(X,idx,axis=0)
    Y = np.delete(Y,idx,axis=0)
    return (X,Y)

def shuffle(X,Y):
    np.random.seed(1337)
    idx = np.arange(len(X))
    np.random.shuffle(idx)
    X = X[idx]
    Y = Y[idx]
    return (X,Y)

### READ CHUNKS GENERATOR ###
def readblock(fp, nlines=100000):
    for i in range(nlines):
        line = fp.readline()
        if not line:
            break 
        yield line


### GET DNN MODEL ###
model = getDNNModel(N_MODS*N_WINS)


X_train = []
Y_train = []
counter=0
with open(F_TRAINX,'r') as xfp, open(F_TRAINY,'r') as yfp:

    while True:
        ### LOAD TEST CHUNK ###
        Y = np.genfromtxt(readblock(yfp), delimiter=",", dtype=config.floatX)
        if Y.size == 0: break
        X = np.genfromtxt(readblock(xfp), delimiter=",", dtype=config.floatX)

        if(len(X_train)==0 and len(Y_train)==0):
                X_train=X
                Y_train=Y
        else:
                X_train=np.append(X_train,X,axis=0)
                Y_train=np.append(Y_train,Y,axis=0)
	print("Loaded %d of lines"%counter)
	counter+=100000


### REMOVE SAMPLES WITH NAN VAL AND SAMPLE SHUFFLING ###
X_train,Y_train = removeNaN(X_train,Y_train)
X_train,Y_train = shuffle(X_train,Y_train)


### TRAIN MODEL ###

t0 = tm.clock();

model.fit(X_train, Y_train,
          batch_size=128,
          nb_epoch=50,
          verbose=1)

print('train time: {time}'.format(time=tm.clock()-t0))
t0 = tm.clock()

### VALIDATE FOR EACH CHUNK IN TEST SET ###
O_write = []
Y_write = []
with open(F_LARGE_TESTX,'r') as xfp, open(F_LARGE_TESTY,'r') as yfp:
    counter = 0
    while True:
        ### LOAD TEST CHUNK
        Y = np.genfromtxt(readblock(yfp), delimiter=",", dtype=config.floatX)
        if Y.size == 0: break
        X = np.genfromtxt(readblock(xfp), delimiter=",", dtype=config.floatX)
        X,Y = removeNaN(X,Y)

        ### MODEL PREDICTION ###
        O = model.predict(X).flatten()
 
        ### WRITE RESULTS ###
        O_write = np.append(O_write,O)
        Y_write = np.append(Y_write,Y)

print('test time: {time}'.format(time=tm.clock()-t0))

### SAVE RAW OUTPUTS
np.save(RAW_F, np.concatenate((O_write.reshape(O_write.size,1),Y_write.reshape(Y_write.size,1)), axis=1))

