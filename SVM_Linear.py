'''
SVM_Linear.py

Installation Requirement: 

scikit-learn

Usage: SVM_Linear.py TrainX_File TrainY_File TestX_File TestY_File Predtion_Output_Path C_Param

Author: Chih-Hao Fang (fang150@purdue.edu)
'''


import sys
from sklearn import svm
import numpy as np
import time as tm





TrainX_file_name = sys.argv[1]  # TrainX_FILE 
TrainY_file_name = sys.argv[2]  # TrainY_FILE
TestX_file_name  = sys.argv[3]  # TestX_FILE
TestY_file_name  = sys.argv[4]  # TestY_FILE
RAW_F		 = sys.argv[5]  # Prediction_OutPut_FILE
C_parameter      = sys.argv[6]  # C


def removeNaN(X,Y):
    idx = np.where(np.all(np.isnan(X), axis=1))
    X = np.delete(X,idx,axis=0)
    Y = np.delete(Y,idx,axis=0)
    return (X,Y)

def shuffle(X,Y):
    np.random.seed(1337)
    idx = np.arange(len(X))
    np.random.shuffle(idx)
    X = X[idx]
    Y = Y[idx]
    return (X,Y)


### READ CHUNKS GENERATOR  ###
def readblock(fp, nlines=100000):
    for i in range(nlines):
        line = fp.readline()
        if not line:
            break 
        yield line


### VALIDATE FOR EACH CHUNK IN TEST SET ###
X_train = []
Y_train = []
counter=0



with open(TrainX_file_name,'r') as xfp , open(TrainY_file_name,'r') as yfp:

    while True:
        ### LOAD TEST CHUNK ###
        Y = np.genfromtxt(readblock(yfp), delimiter=",", dtype=float)
        if Y.size == 0: break
        X = np.genfromtxt(readblock(xfp), delimiter=",", dtype=float)
        #import pdb; pdb.set_trace()
        if(len(X_train)==0 and len(Y_train)==0):
                X_train=X
                Y_train=Y
        else:
                X_train=np.append(X_train,X,axis=0)
                Y_train=np.append(Y_train,Y,axis=0)
	print("Loaded %d of lines"%counter)
	counter+=100000

print("Done loading train file")
X_train,Y_train = removeNaN(X_train,Y_train)
X_train,Y_train = shuffle(X_train,Y_train)

model = svm.LinearSVC(C=float(C_parameter)) 


## Measuring training time ###
t0 = tm.clock();
model.fit(X_train, Y_train)

## Ending Measuring training time ###
print('train time: {time}'.format(time=tm.clock()-t0))
  

### VALIDATE FOR EACH CHUNK IN TEST SET ###
O_write = []
Y_write = []
with open(TestX_file_name,'r') as xfp, open(TestY_file_name,'r') as yfp:
    counter = 0
    while True:
        ### LOAD TEST CHUNK ###
        Y = np.genfromtxt(readblock(yfp), delimiter=",",dtype= float)
        if Y.size == 0: break
        X = np.genfromtxt(readblock(xfp), delimiter=",",dtype= float)
        X,Y = removeNaN(X,Y)
        ### MODEL PREDICTION ###
        O = model.decision_function(X)
 	
        tempt=[]
	for item in O:
		tempt=np.append(tempt,item)
        ### WRITE RESULTS ###
        O_write = np.append(O_write,tempt)
        Y_write = np.append(Y_write,Y)


np.save(RAW_F, np.concatenate((O_write.reshape(O_write.size,1),Y_write.reshape(Y_write.size,1)), axis=1))

